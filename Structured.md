---
title: lightsvc Architecture, Design, Programming Guidelines and Processes:
author: Tiago de Araújo van den Berg
version: SNAPSHOT
date: "2024-09-16"
---

LIGHTSVC PHILOSOPHY, PRINCIPLES, ARCHITECTURE, DESIGN, PROGRAMMING GUIDELINES AND PROCESSES
===========================================================================================

# Preface:

This document is a collection of thoughts, philosophies, practices and processes, gathered over 25 years of experience in my software development career, these reflections are based in my opinion, but they are shared by many colleagues I had the luck to work with along all these years.

In short, my software development journey began with the BASIC programming language. During high school, I moved on to C and Pascal. The web was still in its infancy and would take several more years to become widespread. Before starting college, I was already progressing in my early career, primarily developing programs to interface with communication equipment for a large telecom company, mostly using C. At that point, I had become well-versed in the language with a solid understanding of its practices and patterns.

As expected, the internet was rapidly growing in popularity, and Java was one of the most promising web-ready programming languages at the time when I started college.

That shiny-new, garbage-collected, object-oriented, framework-hungry, pattern-oriented, Java Language, together with all academy and corporation backed heavy-weight processes, standardization and opiniated dogmas typical of late 90s and early 2000s, occupied all of my attention with the promisse to become a "professional" web developer and start have monetary gains.

Somewhat that promisse was fullfilled and I cannot really complain about it, the job market was eager for that type of professional, and I was ready to answer the call, armed with UML diagrams and extensive documentation reports.

For many years, I navigated the Java ecosystem without much questioning about the standards and frameworks widely promoted by major corporations - such as Java EE, Spring, and Hibernate (that I was very knowledgeable of, and loved to hate), just to name a few.

In some technological wanderings sometimes I stumbled with precious pieces of software that show that it was possible to use other paradigms in Java. [Spark](https://github.com/perwendel/spark) (The Web Server/Router One, not the Apache big data analytics) being one of more influential ones.

After years working exclusively with Java I started to play with some new technologies, like the Go Programming Language. I fell in love immediately by many of its philosophies and overall simplicity. Many of those philosophies I experienced before and was familiarized long ago in languages like C and Pascal. 

More recently, learning the Go Programming Language, a little of Rust, and studying a bit of the source code from PostgreSQL, those memories and knowlegde lost long ago came back to my memory. And like magic, lots of the fundamentals like the primitive error handling of C, with error codes and its constants to reference them, and the modernized version of error handling found in Go and Rust, where errors are as simple as returned values of functions. In these examples, errors begging to be handled immediatelly after the function call, no stacktrace by any means. 

These are a stark contrast to the common exception handling patterns found in Java applications, centralized, in the top of the execution stack, logging monstrous piles of stacktrace, to the despair of developers and systems operators. 

Comparing those "almost opposite" strategies, it became clear that the old ways of C, where a single error code was mapped uniquely to an error handling in code and all the error code transformations all way up in the function call stack, is a much superior strategy of handle errors. It is auditable and traceble with a simple number, No exception with a stacktrace can beat that. 

That experience made me reconsider everything I thought I could have "unlearned" or missed all those years in this ordeal with Java, and I became eager to revisit that knowledge with a new perspective and fresh insight.

To ilustrate this: I always had the felling that something was off when coding Simple Java POJOs and using accessor methods (getters and setters). I always suspect that POJOs are more a data structure than an object (or kinda of a Pure Fabrication abstraction of that data structure). All those accessors, and the object abstraction per se, looked like boilerplate, so unnecessary that the Java community created a code injectors (like Lomboz) to create those accessor methods instead of using those attributes directly. Now it become clear that was a dogma (OOP purism in this instance). That dogma and many others permeate the Java Ecosystem, like the need of an ORM (Did I say that I hate Hibernate?), and a tendency to use unnecessary patterns and frameworks.

In the past, a coworker, one highly regarded of me, said that each programming Language has its own culture and dogmas and that is an error to code in a language with the standards of another. I couldn't and can't disagree more.

Now, even naming patterns like the upper case naming of constants and enums are challenged if it presents any advantage over the established standard or dogma. Eg: Go language uses a different naming standard for constants than Java. So, Go constant naming is wrong? Is there any way that is wrong? No, but there are advantages and/or disadvantages when comparing such standards.

The main reasoning behind the creation of this document is to experiment new ways of develop software within the Java Programming Language Ecosystem. 

There is not a single original idea of mine in this document, all ideas were gathered through the years, mostly from observation of other people work (or simply code "stealing"), forums, some books or articles, instructional or events attendances videos, simple deduction and practice. 

Many of the patterns, processes or principles described here, have other known names and were formally described/invented by someone else and I did not know the names or authors of these ideas. I apologize for that. If you find any uncredited idea, please contact me and I will correct that.


# The Humane Aspects: Development Philosophies

These are some mindset and practices that developers adopt throughout the software development process. They are about how we approach problem-solving, collaboration, adaptability, and decision-making.

Many of these philosophies help reinforce other ones.

## Openness to (Negative) Feedback

### - Ask for feedback constantly: 
The faster you have feedback, the faster you can fix things along the way before you have to redo a lot of work. "When fail, fail fast.", so you can change course an try another approach to solve the problem at hand. 

Keep the scope of feedback small, so it is more easy to focus on the exact negative aspect of a solution when it happens.


### - Do not let feedback hurt your feelings, Do not hurt the feelings of someone:
Many times when receiving feedback, about our work, we tend to handle it like it was something personal, we are human after all, we have feelings, and we don't like when people or machines "attack" something we made with love and care. 

We need to understand that a feedback, given or taken, is about the work in question, is not about the author.

In the other hand, we cannot let any issue became something to ashame people. A red icon in a failed pipeline in a  


### - The fast feedback loop
In any aspect of software development: Coding, Processes or Professional feedback. Try to receive feedback as fast as possible.

A feedback loop is the cycle where the results of actions are evaluated, and adjustments are made based on that evaluation. The faster the feedback loop, the quicker issues can be identified and risks mitigated, learning from them can be made, and improvement adaptation follows.

A smaller scope of feedback avoid the rise of **negativity bias**, the psychological phenomenon where, given an experience full of aspects, individuals give more weight to negative aspects than positive ones, even when the positives may outweigh the negatives.


### - Pick what is useful and discard what is not: 
People have acquire many biases, their experiences made them saw the same thing through other perspectives, some of them truly enlightened, some of them pretty limited.

Do not receive and follow any negative feedback blindly, learn to extract the useful feedback, ignore the rant and [**dogmas**](#dogma-free-programming).

### - Document The Issues And Lessons Learned, And Share Them Publicly

Documenting the issues and lessons learned ensures that they are not forgotten. Instead of moving past an issue after fixing it, teams can capture key details about the problem, its root cause, and how it was resolved. The result ends creating a powerful knowledge base that others can reference. New team members or contributors can quickly get up to speed by reviewing past mistakes and solutions, reducing onboarding time and enhancing knowledge retention across the team.

Public sharing also invites feedback and further discussion, which can offer additional insights or solutions that you might not have considered.

As an example, this document serves as an excellent resource for recording such issues and their solutions. Even better, at times, an issue can be captured directly in an [Executable Specification Test](#executable-specification-test).

//TODO

## Dogma-free programming

Dogma-free programming refers to a programming approach that avoids rigid adherence to particular ideologies, rules, or "best practices" without question. In this approach, programmers are encouraged to be pragmatic and flexible, choosing the best tools, techniques, and methodologies based on the specific requirements of a project rather than sticking to any specific programming paradigm or set of conventions for the sake of it. 

Some aspects of dogma-free programming include:


### - Pragmatism over ideology:
Instead of strictly following a certain programming paradigm (e.g., object-oriented, functional, or imperative) or specific framework, a dogma-free programmer selects the tools that best suit the task at hand.


### - Open-mindedness: 
Dogma-free programming encourages exploring different approaches, frameworks, languages, or techniques, even if they fall outside one's usual preferences or training.


### - Context-driven decisions: 
Every project and problem is different, so decisions regarding architecture, code structure, and technology should be driven by the specific needs of the project, not by preconceived notions or trends.


### - Resilience to trends: 
A dogma-free programmer avoids being swept up by hype-driven development (e.g., adopting a technology just because it's trendy) and critically evaluates whether new tools and methodologies offer real value.


### - Focus on results: 
The ultimate goal is to produce working, maintainable, and efficient software. If this requires bending or even breaking certain conventions, that's considered acceptable in dogma-free programming.


## Communication is Paramount

//TODO

### - When possible, preffer real time communication

//TODO


## Code Caring and Owneship

//TODO

### - Get the people aboard

//TODO

### - Convince everyone

//TODO

## Pair Programming

//TODO

## Develop software in-depth, not in-breadth

//TODO

## Continuous Delivery

//TODO 

### - Heavy usage of automation
### - Pipelines


## Test Your Code

### - Test Driven Development
### - Executable Specification Test
### - Test Before Fix
### - No Code Versioning Branches
### - Heavy Usage of Feature Toggle/Feature Flags
### - Configuration as Code
##### Configuration as YAML files

//TODO

## Keep It Stupid Simple (KISS)

The core idea behind the KISS principle is that simplicity should be a key goal in design, and unnecessary complexity should be avoided. 

It is one of the "Zen of Python" principles in the saying "Simple is Better Than Complex".

This principle lean towards the use of Procedural Programming and Functional Programming as they are usually the simpler, shorter way to solve many specific problems [without the need to create abstractions](#functions-are-better-than-oop-abstractions).

## You Aren't Gonna Need it (YAGNI)

The idea behind YAGNI is that developers should only implement functionality when it is actually needed and not preemptively add features or code that might be required in the future.

## Aggressive Componentization and Reuse

//TODO Complex is Better than Complicated
//TODO Complexity comes in Layers
//Java Hash Example

## The Right Tool For The Right Job

This principle emphasizes the use of the the most appropriate tool, technique, or method for each specific situation, rather than relying on a single, familiar solution for every problem. This principle encourages flexibility, critical thinking, and the understanding that different problems require different approaches. 

It is a counterpoint to the infamous "Golden Hammer" problem, with its fammou essay: *"If all you have is a hammer, everything looks like a nail.".*. This refers to the tendency of individuals to rely too heavily on a familiar tool, method, or technology, even when it's not the best choice for the problem at hand. Worse yet, sometimes a tool is choosen simply by the [established dogmas](#dogma-free-programming). 

In most cases the right tool is the simplest and cheapest, not the one readily available.

## Explicit is better than implicit

One of the "Zen of Python" Guiding principles, it emphasizes clarity and transparency in code. This means that it's generally better to write code where the behavior, functionality, and intentions are clear, rather than relying on hidden or indirect mechanisms that might obscure what the code is doing, making sure the meaning of your code is easy to understand at a glance, even if it requires more verbosity or repetition. It improves readability, maintenance and avoid surprises.

The use of functions made the behavior explicit while reuse the code!

## Don't Repeat Yourself (DRY)

Emphasizes reducing redundancy in code by avoiding duplication. The idea is that every piece of knowledge or logic in a program should be represented once and only once, ensuring that the codebase remains maintainable, consistent, and less prone to bugs when changes are made.

At first glance, this principles may appear to be in conflict with the "Explicit is better than implicit" principle. For example, making code more explicit could seem like it might lead to repetition, and adhering strictly to DRY could push you towards more implicit behaviors by abstracting common logic into shared components. 

In most cases, explicit repetition is acceptable when it improves clarity, even though it might seem to violate DRY. If abstraction makes the code less understandable or ambiguous, keeping certain code duplicated (or slightly repetitive) can be more beneficial.

## The Model Is King.

Following Domain-Driven-Design (DDD) definitions, The Model is about business rules, state machines, operations and finally data structure in this order of importance.

### - Bounded Context 

//TODO

### - Communication throught code

//TODO

### - Communication throught documentation

//TODO

### - Communication throught tests

//TODO

### - Common Vocabulary

//TODO

# Software Architecture

## Microservices

[Microservices](https://en.wikipedia.org/wiki/Microservices)

//TODO

### - Use technology-agnostic protocols

//TODO

### - Microservices is not just about network distributed applications, It is about the software structure

### Microservices Ensure The Bounded Context of the Model

### Microservices avoids the collateral damage of cascadind software modifications

## Three-tier Architecture plus a Model Tier

Three-tier architecture is a specialization of the [Multitier architecture](https://en.wikipedia.org/wiki/Multitier_architecture) software design pattern that separates an application into three distinct tiers, or layer. Each layer has its own responsibility, making the system more modular, scalable, and maintainable.

In this document we refer to the classic three-tier architecture, with the following layers defined:

### - Presentation Layer

### - Application Logic Layer

### - Data Access Layer


## Functional Programming

### Functions are better than Annotations

 Functions offer a simple and direct way of expressing operations, focusing on inputs and outputs and a well defined outcome.

 Functions are easier to test and they are phenomenally [explicit](#explicit-is-better-than-implicit).

### Functions are better than Pure Fabrication OOP Abstractions

Object Oriented Programming excels at modelling real-world concepts and entities, but it fall short in a "Pure Fabrication" concept, where it is designed solely to fulfill a software need (e.g., improving cohesion or decoupling logic), performing tasks without being tied directly to the domain model. 

Using OOP to fulfill those abstract tasks and concepts creates unnecessary boilerplate code and creates complexity and abstractions where they are not needed.

Functions are much more simpler, when all you need is perform a "function" of your software (Sorry for the pun).





# Coding Practices

## Flat is better than nested.




