---
title: "lightsvc Architecture, Design, Programming Guidelines and Processes"
author: "Tiago van den Berg"
date: "2022-12-20"
---

# LIGHTSVC ARCHITECTURE, DESIGN, PROGRAMMING GUIDELINES AND PROCESSES
================================

**Author:** *Tiago van den Berg*

---

# Table of Contents


---

# Reasoning for one more "framework"/"component"/"process"/"pattern"
 - This is not a framework, but a set of components and practices.
 - Previous experience with Java projects.
 - Highly influenced by Golang implementation and its philosophy overall simplicity.

---

# Architecture and Design Overview 

Design Principles:
 - Keep it simple. The Zen of Python, Yagni, DRY, TDA (Law of Demeter), Avoid the Golden Hammer.
 - Dogma Free Programming
 - Functional-Programming over Object-Oriented-Programming.
   Especially over pure fabrication classes. Less so in model entoties.
 - [Multitier architecture](#multitier-architecture)
 - [Microservices](#microservices)

---

# Dogma-free programming
 Dogma-free programming** refers to a programming approach that avoids rigid adherence to particular ideologies, rules, or "best practices" without question. In this approach, programmers are encouraged to be pragmatic and flexible, choosing the best tools, techniques, and methodologies based on the specific requirements of a project rather than sticking to any specific programming paradigm or set of conventions for the sake of it.

Key aspects of dogma-free programming include:

1. **Pragmatism over ideology**: Instead of strictly following a certain programming paradigm (e.g., object-oriented, functional, or imperative) or specific framework, a dogma-free programmer selects the tools that best suit the task at hand.

2. **Open-mindedness**: Dogma-free programming encourages exploring different approaches, frameworks, languages, or techniques, even if they fall outside one's usual preferences or training.

3. **Context-driven decisions**: Every project and problem is different, so decisions regarding architecture, code structure, and technology should be driven by the specific needs of the project, not by preconceived notions or trends.

4. **Resilience to trends**: A dogma-free programmer avoids being swept up by hype-driven development (e.g., adopting a technology just because it's trendy) and critically evaluates whether new tools and methodologies offer real value.

5. **Focus on results**: The ultimate goal is to produce working, maintainable, and efficient software. If this requires bending or even breaking certain conventions, that's considered acceptable in dogma-free programming.

This philosophy promotes adaptability, creativity, and continuous learning, as it encourages developers to think critically and be resourceful in their choices rather than sticking rigidly to one way of doing things.


# System name based on function not Marketing name

mail.mycompany.com is better than HyperDuperMail

# Namespace is System name

# The Model is King

Model is about business rules, state machines, operations and finally data structure in this order of importance.

Create different Entities or sub-entities for states preferrably. Eg: Order can be composed of Request, Processing and Deliver associated entities.

Composition vs Aggregation is about Life Cycle of Entities

Composition Stronger vs Weaker Entities

# Model after current requirements.

# Bounded Contexts by microservices and operations

 - That means that there is not only one "Account" Entity

# Model only use Unidirectional Relations

 - The other directionality is achieved by services operations. Eg: retrieveAccountByPhoneNumber

# Separation of state and behavior (Not Object-Oriented)

# SubEntities are Subclasses in Model and DTO
  Avoid use Weak Entities as standlone classes

# Public domain entities attributes

# Nullable domain entities attributes

# Models Entities can be used IN DTO values (all attributes or a subset).

# Do not use Model Entities Directly AS DTOS
  But use the model inside a Request or Response DTO

# Use Model Entities INSIDE a DTO when returning one or a collection of entities or passing a instance
 - That allows you return metadata along side the data
 - Eg: retrieveAccount


# Not all attributes needed to be used or validated in a Model entity Class when used as DTO

# Domain entities attributes individually validable.

# Complete entity validation composed of individual attributes validation.

# All DTO and Model validations are static and public

# All Model Validation contained in a DTO must be Error-Facaded by it.

 - So the error is a DTO error, not a model error

# DTO And Model Validations Methods dependent of external or System Factor should be passed as parameters

 - Configs 
 - Date and Time
 - Previously stored values, as databases records
 - Permissions: Eg: allow store a weak password based on a special permission.

# DTO And Model Attributes validations must follow the same order of its attributes.

# Failed DTO And Model Cross attributes validations must return errors of the last order attributes.
  - That means that an invalid-cross-validation atribute "B" that is in the order after "A", must return a "B" Error.
  - Very common on Authorization vs Domain Attributes

# Model Entities Must Be Comparable

# The Complete Fields Model Constructor is the Main Constructor. All other Constructors must call that.

 - Passing null values or empty collections values.

# All Collections in Model Entity must be initialized

# All Collections in Data Transfer Objects (DTO) must be initialized

# Model Classes Can have a Generic extra attribute that can be used to to be fufilled with other microservices models or stored calculated data.
 
 - Generally a Map<String, Object> attribute


# In DTO when using a large collection of referenced entitys, use it as referenced objects (Flyweight Pattern)

{
  itens: [
    {
      id: 1
      name: Foo
      clientId: "abc"   // This is a reference for clients
      scope: "xyz" // This is a reference for scopes
    }
    {
      id: 1
      name: Bar
      clientId: "abc"   // This is a reference for clients
      scope: "mno" // This is a reference for scopes
    }
  ],
  referencedClients: {
    abc: {
      name: Client ABC
      description: A very big description...
    }
  },
  referencedScopes: {
    xyz: {
      name: Scope XYZ
      description: A very big description...
    },
    mno: {
      name: Scope MNO
      description: A very big description...
    },
  }

}


# Prepare for real future requirements.

# Keep it simple. 

# The Zen of Python.

# English as the Only Language Used in the Application.

# Teams Organizations

Everyone must knows ( or access, or work with ) everything. Work must be decoupled.

Specialization leads to team coupling (Everyone must work in every feature)

# Twelve Factors Application

# Code Versioning

# Version Control

# Explicit Dependencies on Application and Environment

# The right tool for the job is the simplest and cheapest.

In terms of features, guarantees and performance, not easy of usage.

# Multitier architecture

[Multitier architecture](https://en.wikipedia.org/wiki/Multitier_architecture)

Design Principles:
 - Presentation Layer
 - Application Logic Layer
 - Data Access Layer
 - Do not skip architectural layers.
 - Architectural Layers only access below layers.
 - Communication with other microservices (remote or not) are made through Data Access Layer

# Microservices

[Microservices](https://en.wikipedia.org/wiki/Microservices)

Design Principles:
 - Development Environment Agregated Servers And Proxy

Implementation Details:

# Functional Programming over Object-Oriented-Programming

Design Principles:

Implementation Details:
 - Forbiden Constructor on Utilitary Static Methods Classes.

# Classes Names as Namespaces

# Unit Tests

Design Principles:
 - Test-Driven-Development

Implementation Details:
 - Temporal Values provided by the Applications are provided by Injected Functions

# Test Driven Development

Design Principles:
 - Test-Driven-Development

Implementation Details:

# Executable Specification Test

Format: method_mustXXXXXX_whenYYYYYYYYYY
Test Input Variables Naming:

Design Principles:

Implementation Details:

# Test Before Fix

Design Principles:
 - Test-Driven-Development

Implementation Details:

# Pair Programing

# Work in Depth instead of Width

# Code Caring and Code Ownership

# Continuous Delivery

# Heavy usage of automation

# Deployment Pipeline

# No Code Versioning Branches

# Heavy Usage of Feature Toggle/Feature Flags

# Configuration as Code

# Configuration as YAML files

# Configuration YAML Files accept references

# Configuration YAML Files Accept Values as Environment Variables

# Execution Environment Config as Enviroment Variables

 - URL Prefixes or suffixes
 - Other variables completelly associated with the execution environment instance.

# Configuration YAML Files Accept Values as text files

# Configuration YAML Files Accepts Encrypted Secret Values with Enviroment Variables or External Files as Keys

# A publishing is composed of application and config version deployments in a execution environment instance.

# Both Application Deployment and Config Deployment Fires a Process Execution (Publishing) in a Execution Environment instance

# Application must be capable of validate the configuration

# Publishing fails in pipelline if Application fails to validate the Configuration.

# Application Initialization fails if Application fails to validate the Configuration.

# Change the default Timezone of the application to UTC
 - Avoid problems with database drivers and databases server configurations

# Liveness and Readiness Endpoints

# Feature Flags in a Features Flags Group VS Feature Flags in each specific Feature Group

  featureFlags:
    enabledEmail: true
    enabledSms: true
  notification:
    mail:
      senderAddress: do-not-respond@example.com
      host: smtp.example.com
      port: 25

  VS

  notification:
    mail:
      enabled: true
      senderAddress: do-not-respond@example.com
      host: smtp.example.com
      port: 25
    enableSms: true

# Component should support Mocks, Dry runs and other test features always that is possible.

 - The configuration must be VERY explicit and not accept such features in production environments.

# No use of Dependency Injection Framework

Design Principles:

Implementation Details:
 - Dependency Injection as Constructor Injection

# Dependency Injection as Constructor Injection

# Use Dependency Injection Methods only to change The Default Composition Construction

# Dependency Injection Methods should be identified as "inject" methods instead of setter.

# External Module Dependencies on Constructor

 - Usually a Map<String, Object> with all external dependencies.

# Modules Types

As most software systems nowadays, there is a multitude of different modules, each one with their own objectives and purposes. So it is normal that, for each 
one of these purposes, there is a different solution and therefore a different set of architecture and design solutions. 

Said that, many modules share some architetural and design features or philosophies.

In each module type, the architecture and design will be detailed further.

Below are the module types identified:

 - [Service API Module (API)](#service-api-module-api);
 - [Backend-For-Frontend Module (BFF)](#backend-for-frontend-module-bff);
 - [Client-Side-Rendering Web Frontend (CSR)](#client-side-rendering-web-frontend-csr);
 - [Server-Side-Rendering Web Frontend (SSR)](#server-side-rendering-web-frontend-ssr);
 - [Feature Component Module](#feature-component-module);
 - [External Service API Client Module](#external-service-api-client-module);
 - [Utility Library Module](#utility-library-module);
 - [Web Related Components Module (HTML, CSS, Javacript, WebComponents)](#web-related-components-module-html-css-javacript-webcomponents);
 - [Web Repositories Module (CDN)](#web-repositories-module-cdn);
 - [Configuration Repository Module](#configuration-repository-module)
 - [Local Developer Launcher Module](#local-developer-launcher-module)

---

# Service API Module (API)

For simplification and standartization of communication the acronym "API", when used alone, is used in this documentation to refer Service API Modules.

Service API Modules, represents the services (as in [Service-oriented Architecture](https://en.wikipedia.org/wiki/Service-oriented_architecture)) where the 
containing all the system and domain logic (and all its rules) and where business operations are executed. 

The Service API Modules, similarly to [Backend-For-Frontend (BFF)](#backend-for-frontend-module-bff), utilizes a [microservices](#microservices) oriented, 
5 [multitier architecture](#multitier-architecture) layers. 

The Layers are:
  - [Presentation Layer](#presentation-layer);
  - [Application Logic Layer](#application-logic-layer);
  - [Data Access Layer](#data-access-layer);
  - [Domain Model Entity Layer](#domain-model-entity-layer);
  - [Application Layer](#application-layer).

Unlike a BFF module, an API module DOES NOT control User Session State and uses Bearer Tokens directly for authentication purpose (unless in an unauthenticated 
operation).

Most Services APIs, persists the execution of their operations in databases or call another services responsible to persist such data. This function is used 
through the [Data Access Layer](#data-access-layer) aggregation of Database Classes or API Clients.

Service API Module Organization: 

To allow an easy integration with other modules (API and BFF) that may depend on such modules, and to allow switch between remote calls or embedded local 
calls in a single executable package, the [Service API Modules Splits Application Logic in API, API Server, API Client and Maven aggregator components](#service-api-modules-splits-application-logic-in-api-api-server-api-client-and-maven-aggregator-components). 
Such projects are maintained in a [Common Code Versioning Repository for Application Logic API, API Server, API Client and Maven aggregator components](#common-code-versioning-repository-for-application-logic-api-api-server-api-client-and-maven-aggregator-components)

Design Principles:
 - REST, Clean URLs and Query parameters usage
 - Service API Versioning in URL
 - Service API Versioning in main package namespace
 - JSON over HTTP
 - [Service API Modules Splits Application Logic in API, API Server, API Client and Maven aggregator components](#service-api-modules-splits-application-logic-in-api-api-server-api-client-and-maven-aggregator-components)
 - [Common Code Versioning Repository for Application Logic API, API Server, API Client and Maven aggregator components](#common-code-versioning-repository-for-application-logic-api-api-server-api-client-and-maven-aggregator-components)
  ---

# Backend-For-Frontend Module (BFF)

For simplification and standartization of communication the acronym "BFF", when used alone, is used in this documentation to refer Backend-For-Frontend Modules.

Backend-For-Frontend Modules are APIs, created and designed to ease development of frontends, usually by the means of been specifically tailored to serve user 
interfaces and interactions and their specific needs. 

For example, sometimes a user interface need to shows data gathered from various services (commonly [Service API Modules (API)](#service-api-module-api))) and 
additionally that data need to be processed (merged or transformed) to be presented in an user interface. The BFF fullfill that role, retrieving and processing 
all the data necessary to that user interface, leaving a few or no responsabilities beyond present the data to the user. The [auditing](#auditing) is simplified
too by having each user interaction logged individually.

Furthermore, the BFFs to handle all user session and authorization responsabilities, commonly through [Cookies with encrypted Bearer Tokens as 
authorization Mechanism](#cookies-with-encrypted-bearer-tokens-as-authorization-mechanism) instead of [Bearer Token on HTTP Authorization Header as Authorization Mechanism](bearer-token-on-http-authorization-header-has-authorization-echanism)) commonly seen on Service API operations.

For most cases, it is designed around the principle that for each page that shows data or for each operation (an user interation) in frontend, there is one 
single operation in the backend. Because of this one-to-one mapping the [BFF Operations and Structure Naming Follow The User Interface Paradigm](bff-operations-and-structure-naming-follow-the-user-interface-paradigm).

That said, nothing phohibits to load certain portion of pages dynamically, in fact for specific scenarios that is the only way to implement certain features (
eg. searchs, items detailings, etc.). The BFF appproach is not recommended (or at least do not presents great benefits) for applications where the majority of 
operations are partial updates to user interfaces, (eg: IDEs, Graph monitoring, etc) because most pages can be fully updated using such partial data retrievings 
operations.

Optionally, some local validations on frontend can be done to avoid calling services over the network.

The Backend-for-Frontend Modules, similarly to [Service API Module (API)](#service-api-module-api), utilizes a [microservices](#microservices) oriented, 
5 [multitier architecture](#multitier-architecture) layers. 

The Layers are:
  - [Presentation Layer](#presentation-layer);
  - [Application Logic Layer](#application-logic-layer);
  - [Data Access Layer](#data-access-layer);
  - [Domain layer](#domain-layer);
  - [Application Layer](#application-layer).

A BFF has many similarities to a [Service API Module (API)](#service-api-module-api), but due to its high coupling with the 
[Client-Side-Rendering Web Frontend (CSR)](#client-side-rendering-web-frontend-csr) that it servers, it is considered a good practice to maintain both the 
BFF and CSR modules on the same versioning control repository. Because of this sole purpose to serve the CSR module (not other modules), it is not necessary, 
like a Service API Module, split the project in clients and server projects, keeping only the server part.

In a BFF module the [Data Access Layer](#data-access-layer) mostly aggregate remote Service API Modules operations calls and rarely have direct access to a
database. BFF often maintais Session or Flow Control and keeps the Access Tokens needed to access in these mechanisms to call proper 
[Service API Module (API)](#service-api-module-api).

Responsabilities:

Design Principles:
 - Frontend is a Simple View Layer
 - REST, Clean URLs and Query parameters usage
 - BFF Should not have Versioning in URL
 - BFF not need versioning in package
 - JSON over HTTP
 - Synchronization of URLs between frontend and backend.
 - Cookies with encrypted Bearer Tokens as Authorization Mechanism

Implementation Details:

# Client-Side-Rendering Web Frontend (CSR)

While pages that does not shows data and operations that not change data, or process only local data, do not interact with 
backend at all.

Responsabilities:

Design Principles:
 - Frontend is a Simple View Layer
 - REST, Clean URLs and Query parameters usage
 - BFF Should not have Versioning in URL
 - BFF not need versioning in package
 - JSON over HTTP
 - Synchronization of URLs between frontend and backend.

Implementation Details:

# Server-Side-Rendering Web Frontend (SSR)

Responsabilities:

Design Principles:
 - Frontend is a Simple View Layer
 - REST, Clean URLs and Query parameters usage
 - BFF Should not have Versioning in URL
 - BFF should not need versioning in package
 - JSON over HTTP
 - Synchronization of URLs between frontend and backend.

Implementation Details:

# Feature Component Module

Responsabilities:

Design Principles:

Implementation Details:

# Utility Library Module

Responsabilities:

Design Principles:

Implementation Details:

# Web Related Components Module (HTML, CSS, Javacript, WebComponents)

Responsabilities:

Design Principles:

Implementation Details:

# Web Repositories Module (CDN)

Responsabilities:

Design Principles:

Implementation Details:

# Configuration Repository Module

Responsabilities:

Design Principles:

Implementation Details:

# Local Developer Launcher Module

Responsabilities:

Design Principles:

Implementation Details:

# External Service API Client Module

# Presentation Layer

Responsabilities:
 - Receive Command-Line-Interface requests;
 - Receive client applications or browser requests;
 - HTTP REST Endpoint Mapping to Application Logic Operations (HTTP Methods and Paths and Optionally Query Modifiers and Hosts)
 - Convert presentation protocol input parameters to Application Logic Operation Parameters (HTTP Path Parameters, Query Parameters, Headers and JSON Payload)
 - Convert the Application Logic return value(s) to the presentation protocol (HTTP Status Code, Headers and JSON Payload).
 - Handle the Application Logic Service Operations Errors and convert them to the presentation protocol (RFC 7807).
 - User Session state. (In Backend for Frontend)
 - Authentication Initiator
 - Documentation of API in the presentation protocol format (OpenAPI).
 - Internationalization
 - HATEOAS

 Design Principles:
 - Architectural Layers only access below layers.
 - Do not skip architectural layers.
 - HTTP as main communication protocol
 - REST, Clean URLs and Query parameters usage
 - Service API Versioning in URL
 - BFF Should not have Versioning in URL
 - Stateless Backends
 - Metrics on service boundaries
 - Separation of Logic Session from Presentation Session
 - Presentation Session as Storage for Logic Session
 - Stateless Short Session

Implementation Details:
 - Structure of Application
 - Initialization
 - Naming
 - JSON as request and response serialization
 - Problem Details for HTTP APIs (RFC7807)
 - Query parameters and application/x-www-form-urlencoded content as request serialization
 - HTML content as response serialization
 - All fields are protected
 - All methods are public
 - Mustache as Server-Side-Rendering Template Engine
 - Backend-For-Frontend (BFF) and Client-side-rendering Web Frontend on same repository.

# Application Logic Layer
 Responsabilities:
 - Service Operations Definitions (CRUD, Use case Operations)
 - Data-Transfer-Objects Definitions (based on Service Operations)
 - Implementation of Services Business Rules
 - Create Transaction Boundaries
 - Auditing
 - Session Management, where it applies

Design Principles
 - Architectural Layers only access below layers.
 - Do not skip architectural layers.
 - Communication with other microservices (remote or not) are made through Data Access Layer
 - Idempotence on Service APIs.
 - No knowledge of presentation mechanisms or features (Presentation-influence-free).
 - Application Logic Request and Response Objects
 - Authentication is responsability of Application Logic Layer
 - Web to Application Logic Request and Response Objects Mapping
 - Unique Errors and Error Facade
 - Errors names are rules names
 - Method errors as enumerations
 - Transactions are not propagated to remote APIs
 - Temporal Values provided by the Applications are provided by Injected Functions
 - Service API Modules Splits Application Logic in API, API Server, API Client and Maven aggregator projects
 - [Common Code Versioning Repository for Application Logic API, API Server, API Client and Maven aggregator components](#common-code-versioning-repository-for-application-logic-api-api-server-api-client-and-maven-aggregator-components)
 - Service API Modules Application Logic API Server and API Client implements API
 - Application Logic Operations Request Input Parameters defined in a DTO Object
 - Application Logic Operations Responses defined in a DTO Objects
 - Application Logic Operation Request DTOs, Responses DTO and Errors enumerations must contained in a namespace class named after operation.
 - Application Logic Operation Request DTOs validates all attributes in a method
 - Application Logic Operation Request DTOs validates all attributes using imported Domain Entities attributes validations.
 - API Client operations validate Request Input Parameters before a remote call
 - API Client operations translate remote call errors in local errors
 - API Client operations define and reurns infrastructure remote call errors
 - Pagination on undefined sizes collection responses 
 - Metrics (average time and count) around API Server operations.
 - Metrics (average time and count) around Data Access operations calls.
 - Separation of Logic Session from Presentation Session
 
Implementation Details:
 - Structure of Application
 - Initialization
 - Naming
 - DTO Validation Methods
 - Remote Calls Infrastructure Errors
 - All fields are protected
 - All methods are public

# Data Access Layer
Responsabilities:
 - Persistence
 - Communicate with persistence services
 - Call others microservices
   External API calls are located in this layer 
 - Remote API Clients

Design Principles:
 - Architectural Layers only access below layers.
 - Do not skip architectural layers.
 - Communication with other microservices (remote or not) are made through Data Access Layer
 - Persistence in SQL RDBMS.
 - Use of a SQL Template Engine
 - Consider using Stored Data for Calculated Data
 - Aggregate data access units in a class with public attributes. Do not delegate any operations.
 - Database Access in its own unit as SubClass.
 - Create an Unit for each different used database.
 - Filesystem Access in its own unit
 - Data Access Layers do not handle businness or domain exceptions. 
 - Remote calls throw service and network errors (Infrastructure errors).
 - Metrics on service boundaries
 - Prefer Eventually-Consistence over Two-Phase-Commit
 - Application must support multiple Databases
 - Database replication with 1 main and many secondaries
 - Read-only Access prefer use of secondary databases


Implementation Details:
 - Structure of Application
 - Initialization
 - Naming
 - All fields are protected
 - All methods are public

# Domain Model Entity Layer
Responsabilities:
 - Entities
 - Validation (Field and Entity)
 - Cross referenced by Presentation, Application and Data Access Layers

Design Principles:
 - Bounded Contexts by microservices and operations
 - Model only use Unidirectional Relations
 - Separation of state and behavior (Not Object-Oriented)
 - Public domain entities attributes
 - Nullable domain entities attributes
 - Entities can be used as DTO values (all attributes or a subset).
 - Domain entities attributes individually validable.
 - Complete entity validation composed of individual attributes validation.
 - All validations static and public
 - Entities must implement Comparable Interface and equals and hashCode methods.
 - Unique Errors and Error Facade
 - Errors names are rules names
 - Method errors as enumerations
 - All Collections in Model Entity must be initialized

 Implementation Details:
  - Entity Naming
  - Validation Methods
  - Temporal Fields Types
  - Temporal Fields Naming
  - Temporal Fields must be Stored in UTC Timezone
  - All fields are protected
  - All methods are public

# Application Layer
Responsabilities:
 - Create and assemble the application structure.
 - Define all the components dependencies.
 - Define the Configuration (based on application structure)
 - Define the application defaults
 - Document the command line interface usage and options
 - Initialize the application
 - Command Line Interface and CLI Help
 - Database DDL initialization in Application
 - Database DDL and DML Update in Application
 
 Design Principles:
 - Configuration as Code
 - Sensible Defaults
 - Runtime documentation embedded in the application
 - Avoid use of builders
   In Components with an activation phase (eg: start() method), prefer simple constructors with ONLY mandatory COMMENTED parameters.  

Implementation Details: 
 - Structure of Application
 - Initialization
 - All fields are protected
 - All methods are public

# Sensible Defaults

# Validate all inputs in a public method, sometimes in private methods too

# Use getter and setter methods without prefixes.

# Errors names are rules names
  "Must (Not) Be", "Must (Not) Have", "Must (Not) Match", "Must (Not) Return" Type Errors

# Application Logic Request and Response Objects
- Data Transfer Object (DTO)
- Naming
- Web to Application Logic Request and Response Objects Mapping

Always use it even if there is only (almost impossible) one attribute in the request or response.

It is an enabler for the Command Pattern (GOF Pattern).

Must contains all neccessary information inside it (Even authentication/Authorization Data).

# Authentication is responsability of Application Logic Layer

# Web to Application Logic Request and Response Objects Mapping

# Avoid use of error stacktrace to find problems.

# Unique Errors
  - Improved error handling
  - Improved auditing 

# Use infrastructure role names instead of implementations or product names in Error naming

# Error Facade
  The error handling paradox:

# Using Enumerations as Unique Error Values
  The java.lang.Exception as subclasses problem in a microservices architecture:

# Using Error Enumerations with lowerSnake_camelCase instead of UPPER_SNAKE_CASE

# Using Exception Translation Pattern as Error Facade

# Error values are always relative to input parameters of the method
 - Not relative to Stored or Config values
 - "request_" parameter when using action or command pattern
 - Eg: Still "request_accountId_mustXXX" even when accountId was derived from account.

# Exception Cause Chain are only necessary when there is more than one possible cause

# Minimize error handling scope
  - Handle errors for each operation call individually. 

# Errors shown to users are uniquely identifiable with an user error code

# Generate user error codes automatically

# HTTP APIs Calls must follow RFC 7807 Error reporting.

# REST, Clean URLs and Query parameters usage

Query Parameters only to modify a resource presentation, projection (filter fields/attributes), filter (query or category), pagination and sort collections.

Those record filters/attribute filters can indicate a new operation. These operations can have diferent permissions.

Use Noums for resources not verbs.

Use Plural noums for collections, singular for items

RFC 7231
https://martinfowler.com/articles/richardsonMaturityModel.html
https://www.crummy.com/writing/speaking/2008-QCon/act3.html

GET 	collection 	Retrieve all resources in a collection
GET 	resource 	Retrieve a single resource
HEAD 	collection 	Retrieve all resources in a collection (header only)
HEAD 	resource 	Retrieve a single resource (header only)
POST 	collection 	Create a new resource with artificial identifier in a collection, return the URL of new resouce in Location header
PUT 	resource 	Create an resource with a natural identifier or Update a resource with an already created identifier (natural or artificial)
PATCH 	resource 	Update a resource
DELETE 	resource 	Delete a resource
OPTIONS 	any 	Return available HTTP methods and other options

Remove trailing slash from URLs

204 Status code is meant for the User-agent not follow the uri and stay in the current state/page/url depending on the Referer page. Do not use it to indicate a operation has no body. use code 200 and header Content-Length: 0 to indicate there is no body in the response.

205 Status code is meant for the User-agent refresh the Referer page is some case. Do not use it to indicate another resource needs to invalidate cache or anything. use code 200 and headers designed for this function to indicate there is a need to invalidate client cache.

401 means the access is denied but if the user sends authorization information or reauthenticates with the same credential, the resource could be available.

403 means even if the user reauthenticate, in the current user state, the resource access will continue to be denied.

Enable Operations as PUT methods and Disable Operations as DELETE methods

# Model REST Operations after Model Operations. REST is not only for CRUD.

 - Model State Changes Mapping

# Use HTTP2 

# Keep then HTTP2 communication/socket open and alive using pools.

# Semantic Versioning For Libraries

# Rolling Versioning for Service API Modules and BFF Modules

# Service API Versioning in URL

# Service API Versioning in main package namespace

# Never break compatibility in API methods and operation

 - Create another version of API if necessary or Create another method
 - Always maintain compatibility

# A Service API can serve (and/or translate) legacy API versions

# Newer Service API versions must have all operations from previous versions if such operations stil apply (Avoid versioning by operation)

# BFF Should not have Versioning in URL

# BFF not need versioning in package

# JSON over HTTP

# Service API Modules Splits Application Logic in API, API Server, API Client and Maven aggregator pojects
Project

# Common Code Versioning Repository for Application Logic API, API Server, API Client and Maven aggregator components

# Packages
 - Naming
 - Roles

# Classes
- Naming
- Roles

# Packages vs Classes Namespaces

# Atributes
- Naming
- Roles

# Methods
- Naming
- Roles

# Entities must implement Comparable Interface and equals and hashCode methods

# Temporal Values provided by the Applications are provided by Injected Functions

# Temporal Attributes Types

# Temporal Attributes Naming

# Temporal Attributes must be Stored in UTC Timezone

# Value of Temporal Fields must be retrieved from Application not the Database

# Persistence in SQL RDBMS.

# Create Database units to aggregate all Database Operations.

 - A SQL file with all queries

# Avoid the Object-Relational-Mapping (ORM) Trap
 - Especially the ones based on annotation/configuration on the DTO/Model/Object file.

# Use of a SQL Template Engine

# Aggregate all Data Access units in an aggregator class with public attributes. Do not delegate any operations from this class.

# When possible Database DAOs must select/insert/update/delete multiple records.

# Common Code Versioning Repository for Frontend and Backend-for-Frontend

# Your Database can be Denormalized, Your Model NOT.

# Model is derived From Requirements and Business Operations, not from Presentation and/or Database.

Eg: If there is a separated enable and disable feature in model that changes a boolean in a table, create one database operation for each (enable and disable). Do not use a generic "update entire entity".

# One Schema for each Microservice

 - Start using one schema by microservice

# Use one Database for each Microservice only when necessary

# Microservices do not access other Microservices Databases/Schemas

# Model Database Operations after Model Operations, Not after Model Entities CRUD.

# Applications execute operations in databases using an unprilvileged user

 - That user does not have permissions to change the database model whatsoever.

# Database DDL initialization scripts in Application

 - These scripts can be parameterized. Eg: execution environment instance values. URLs, passwords, etc.
 - DDL and DML Initialization/Update Files in its own file.

# Database Version check in initialization

 - An explicit version number.
 - A hash of metadata?

# Database DDL and DML Update in Application

 - Use a version table with version column and version migration/creation date.

# Database Timezone check in initialization

 - Compare application timezone vs database timezone.
 - Compare application date and time vs database date and time.
 - Check if both application and database timezone are in UTC

# Database Timezone in UTC

# Manual Database Version Migration

 - Based on version table value

# Full-Text-Search (FTS) on Database

# Database schema naming

 - UPPER_SNAKE_CASE
 - Concatenated System and module name (SYSTEMNAME_MODULENAME)

# Database table naming

 - Upper_Camel_Snake_Case

# Database column naming
 - lower_snake_case

# Prefer Database Natural Keys

# Prefer Database Composed Keys

# Prefer Eventually-Consistence over Two-Phase-Commit

# Read-only transactions when possible

# Minimal scoped transactions possible

# Application must support multiple Databases

# Database replication with 1 main and many secondaries

# Read-only Access prefer use of secondary databases

 - But do not use secondary databases if time constraints between write and subsequent reads (like PRG) is in place.

# Consider using Stored Data for Calculated Data

 - From multiple records or another microservice

  Design Principles:
   - Model Classes Can have a Generic extra attribute that can be used to to be fufilled with other microservices models or stored calculated data.

# Merger/Joiner for multibranch SQLs

# Paginated Queries

# Navigation Object

# Use of Connection Pool

# Database Special Encodings in Columns must state that in Column Name

Eg: key_base64, id_hex, salt_base64, etc

# API Interfaces in a project separated from API Clients

# Create an API Client Library for third party autonomous services nonetheless

# API Operations have only one input parameter: Request

# Application Logic Operations Request Input Parameters defined in a DTO Object

# API Client operations validate Request Input Parameters before a remote call

- Validade Access Tokens only if they are mandatory. Sometimes, authentication are set in config

# API Client operations translate remote call errors in local errors

# API Client operations define and reurns infrastructure remote call errors

# API Client operations can return infrastructure errors in addition to business errors

# If needed, Maintain Calculated Imported Data as a JSON column.

# Avoid crossing microservice data boundaries via database.

# Context Object

# Circuit Breaker

# Use Cache for Resilience

 - In cases where remote resources are unavailable, caches can be used to fullfill the requests

# Caching

# Caches are implemented on the client code, not in infrastructure code, not in server code

# When client misbehave, caches can be implemented in server-side code. Prefer Rate Limits in these cases

# Support API Request cancelation Natively

# Support API Request timeout Natively

# Support API Request Retry Policy Natively

# Support API Request Circuit Breaker Natively

# Support API Request Caching Natively

# Prefer Horizontal Scalability Always that is Possible

# Stateless Backends

# Cookies with encrypted Bearer Tokens as Authorization Mechanism

# User Session stored on Cookies

# User Session stored in-memory data structure store

# Redis as in-memory data structure store

Allow scaling by not creating centralized repositories of state

# Idempotence on Service APIs

# Non-Idempotence on Backend for Frontends

# JWT and JWE as a Stateless Enabling Mechanism

# OpenID and OAuth2 as Authentication and Authorization Provider

# Bearer Token on HTTP Authorization Header as Authorization Mechanism

# JWT or Encrypted Claims (JWE with Symetric Key) as Bearer Token

Encrypted Claims Forces The Client to Validate the Access Token Every time it will be used.

# Use a mechanism or application to rotate encryption keys

Use Encryption Key identifiers on encrypted data

Maintain deprecated keys in Keystore until all valid tokens are expired

# Access Token Scope as Permission

# Split Access Token Scopes in 3 categories (Client Credentials, Client Bound User Access Token and Account Bound User Access Token)

Use a prefix in scope to differentiate between the categories

# Permission by operation and type of access (same_user, same_group, any_user)

Some operations allow the user to change their own data or data that belongs to other users or groups.

In Operations that support both client credentials and user access token permissions, it is necessary to check if user access tokens have permission to access the data.

# RBAC is not enough some times

# Relation Based Access Control (ReBAC) is Better than RBAC
 
 - Pre-Authorization and Post-Authorization
 - RBAC is no able to handle post-authorization

 - Subject (Type + Identifier)
 - Relation (Type + Identifier)
 - Object (Type + Identifier)
 - Many Times over in kind of Recursive Model

# Prefer User Access token over Client Credentials Access Token

 - Even when accessing other people data. (Watch GDPR)

# Scope List Based Permissions

# Scope and Permission Naming based on system_action_resource

system is the internal system name (Not product name)

action on infinitive (Verb)

resource is a substantive

Eg: mysystem_create_person

# Metrics on service boundaries

# Metrics (average time and count) around API Server operations.

# Metrics (average time and count) around Data Access operations calls.

# Auditing

Requirements:
 - Auditing allow redact security, private or legal sensitive information.

Design Principles:
 - Split the operation request, from operation result into two different audit records
 - Fail the operation if audit fails.
 - Auditing is should be focused on forensics, not billing or analytics
 
Implementation Details: 
 - Auditing uses Log Protocol over TCP/IP (RFC 5424 and RFC 6587)

# Auditing allow redact security, private or legal sensitive information.
 - Set fields to redact before trying to read parameters/body/etc. This avoids audit sensitive information in case of read errors.


# Split the operation request, from operation result into two different audit records

# Fail the operation if audit fails.

# Auditing is focused on forensics, not billing or analytics

# Billing and Analytics could be extracted from auditing.

# Billing uses another log separated from Audit.

# Reporting uses another log separated from Audit.

# Reporting intermediary data in a separated Database

# Consolidate Reporting data summaries in batch instead of big queries on each reporting viewer

# Auditing uses Log Protocol over TCP/IP (RFC 5424 and RFC 6587)

# Handled errors in operations are observed in Audit Logs and Metrics, not in application log.

# Auditing must be implemented in Application Logic Layer
 - If Audit is made in Presentation Layer, Java IOException/EOFException errors when HTTP Client disconnects, would flag operation as Fail when in Application Logic Layer, everything was fine.

# Log one event per line

# Use Parseable text log format

# Application Logs are Used Only for Unhandled Errors

# Application Logs are show in application console

# Unhandled errors in Applications are handled and logged in the uppermost Layer/Component/CallStack possible.

# Use of a Log Aggregator for unhandled errors

# Log event attributes

# Code Formatting
 
 - Repeatable formatting
 - Format on commit

# Code commenting

 - No blank lines inside a method
 - Separate functional blocks by a comment

# User Session state. (In Backend for Frontend)

# Separation of Logic Session from Presentation Session

# Presentation Session as Storage for Logic Session

# Stateless Short Session

# Do Not create a Web Session if one already exists.

# Create a New Web Session When User Authenticates.

# OpenID Authentication Client

# OpenID Authentication Server

# Development Environment Aggregated Servers And Proxy

# Production Environment can Aggregate Servers in only one deployment.

# API to Queue Email SMTP Calls

# Warning on Browser console in asciiart

# Discovery Endpoint .well-known/service

# Configuration version is available in discovery endpoint

# Health check endpoint

# Health check endpoint is discoverable through discovery endpoint

# Metrics endpoint

# Prometheus as Metrics format

# Metrics endpoint is discoverable through discovery endpoint

# Documentation
 - Tutorials
 - How-to Guides
 - Explanation
 - Reference

# API Documentation Ruleset

# API Documentation in OpenAPI

# API Documentation endpoint discoverable through .well-known/services

# API Documentation endpoint is executable through SWAGGER

# Models methods not access services

# Reporting Database INSERTS could be asynchronous.

# Validate local validations, then cached resources, then remote resources

# In MVC Flows, Split Request (User Operation) from the Response (State to User View)

# Flow State Change based on previous state operation and result

# Flow between views using flow control base on tokens (encrypted or not)

# Secure By Default

# Console Warning

console.log("%cPARE!!!\n%cNão permita que terceiros utilizem esse console!", "font: 50px sans-serif; font-weight:bold; color:white; background-color: red;", "font: 25px sans-serif; font-weight:bold; color:black; background-color: yellow;");

# Forbiden Constructor on Utilitary Static Methods Classes.

# Content-Security-Policy (CSP) Filters

# Permissions-Policy Filters

# X-Frame-Options Filters

# Cross-Origin-Request-Support (CORS) Filters and Response Utilities

# Heavy Usage of Rate Limiters

# Captcha support

# Internationalization

# Internationalization on Presentation Layer

# Saga Orchestration Transaction Support or Compensation Support

# Visual Identity

# All presentation Aspects in CSS

# All Content Aspects in HTML

# Media content support in CSS to enable Mobile or Alternative Presentations

# HTML Tags id only for Javascript DOM Manipulation

# Vanilla Javascript

# Frontend is a Simple View Layer

  No Complete MVC Layer in Frontend

# Synchronization between frontend URLs and backend URLs

# Transformative methods (perfect funcions) that receive null must return null in most cases.

# Try to flatten the code and remove identation using early exit or early continuation.

 - Heavy usage of continue, break, labels, return and throw keywords instead of nested loop and branch structures.

# Java Interfaces methods must no contains accessors modifiers.

# Variable Naming
 - In Config
 - In App
 - In Logic
 - In method Parameters
 - In Errors Enums
 - In methods Variables
 - Roles not types
 - Sometimes a Variable that is a role name in another system has another role name

# BFF Operations and Structure Naming Follow The User Interface Paradigm

 Eg: CreateAccount.retrieveData.Request, CreateAccount.retrieveData.Response, CreateAccount.saveAccount.Request, CreateAccount.saveAccount.Response

 It is common for a BFF operation Uses a request in a Screen/Page and Respond in another Screen/Page. This is reflected in Request and Responses of one operation:
   - Eg: 
        public ReviewAppliedCurriculum.RetrieveData.Response request saveData(ApplyCurriculum.SaveData.Request request) { ... }

# Cache public static resources in a separated specialist cache module

  location /public-static-resources/ {
      proxy_pass             http://bffhost:bffport/public-static-resources/;
      proxy_cache            cache;
      add_header             X-Cache-Status $upstream_cache_status;
      proxy_cache_valid      any 1m;
      proxy_cache_key        $uri;
      proxy_cache_background_update on;
      proxy_cache_use_stale  error timeout http_500 http_502 http_503 http_504;
      proxy_hide_header      Set-Cookie;
      proxy_ignore_headers   Expires Cache-Control Set-Cookie;
    }

# Public Static Resources should not generate HTTP Session

# A/B Testing Support

# Java Code Patterns
 - Use a Concept Main Class Name as Namespace and create static subclasses
     Ex: Server and Server.Config instead of Server and ServerConfig
 - Qualify Instance variables with this.
 - Getters and Setters using Javascript patterns
     value() and value(newValue) instead getValue() and setValue(newValue)
 - Use of var-args to arrays semantic sugar methods
 - Do Not Use Pattern Names in Class Names as Suffixes or Prefixes. Eg: Do not Use "PersonModel" or "CreatePersonDto" or "AbstractPerson"

# HATEOAS
 - Hypertext as the Engine of the Application
 - Simply put, add operations and URLs relative to the resource in the JSON Response as commands.
 - Links injected in Presentation Layer

# Test Mutiplicity is Zero, One And Two

# Glossary
- API
- BFF
